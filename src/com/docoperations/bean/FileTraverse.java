package com.docoperations.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

public class FileTraverse {
	
	static Properties pros = null;
	
	public static String getRepoName() {
		return pros.getProperty("repo").trim();
	}
	public static String getUserName() {
		return pros.getProperty("user").trim();
	}
	public static String getPassword() {
		return pros.getProperty("pass").trim();
	}
	public static String getSrcDir() {
		return pros.getProperty("srcDir").trim();
	}
	public static String getDesDir() {
		return pros.getProperty("desDir").trim();
	}
	public static String getStrFolderPath() {
		return pros.getProperty("strFolderPath").trim();
	}
	public static String getDestFolderPath() {
		return pros.getProperty("destFolderPath").trim();
	}
	// Get All the Files from the Folder
	public static ArrayList<String>  getAllFiles(String dir) {
		ArrayList<String> listOfFiles = new ArrayList<String>();
		 File fileDir = new File(dir);
		  File[] directoryListing = fileDir.listFiles();
		  System.out.println(directoryListing);
		  if (directoryListing != null) {
			    for (File child : directoryListing) {
			      listOfFiles.add(child.toString().trim());
			    }
		  }
		  return listOfFiles;
	}
	
	public static void getConfigProperties() throws IOException {
		String filePath = "./src/config.properties";
		pros = new Properties();
		FileInputStream ip = new FileInputStream(filePath);
		pros.load(ip);
	}
	public static void main(String[] args) throws IOException {
		FileTraverse.getConfigProperties();
		String srcDir = FileTraverse.getSrcDir();
		System.out.println(FileTraverse.getRepoName());
		FileTraverse.getAllFiles(srcDir);
	}
}
