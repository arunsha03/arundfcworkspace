package com.docoperations.bean;
import com.documentum.fc.client.IDfSession;
import com.documentum.fc.client.IDfSessionManager;
import com.session.*;

import java.io.File;
import java.util.ArrayList;

import com.docoperations.*;
import com.docoperations.dfc.DocOperations;

public class InvokeDocOperations {
	
	static IDfSession session;
	static ArrayList<String> allFilesFromDir = new ArrayList<String>();
	
	public static void main(String[] args) throws Exception {
		
		// Get All the Config Properties value
		
		FileTraverse.getConfigProperties();

		
		SessionDFC sessionDFC = new SessionDFC();
		try {
			session = sessionDFC.createSessionManager(FileTraverse.getRepoName(), FileTraverse.getUserName(), FileTraverse.getPassword());
			allFilesFromDir = FileTraverse.getAllFiles(FileTraverse.getSrcDir());
			for( String file: allFilesFromDir) {
				DocOperations.bulkImport(file, FileTraverse.getDestFolderPath(), session);
				System.out.println(file+" COMPLETED");
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
    		if (session != null) {
    			sessionDFC.releaseSession(session);
                System.err.println("Session Released..!");
    	  }
    	}
	}

}
