package com.docoperations.dfc;

import java.io.File;
import java.util.logging.Logger;

import com.documentum.com.DfClientX;
import com.documentum.com.IDfClientX;
import com.documentum.fc.client.DfClient;
import com.documentum.fc.client.IDfCollection;
import com.documentum.fc.client.IDfFolder;
import com.documentum.fc.client.IDfQuery;
import com.documentum.fc.client.IDfSession;
import com.documentum.fc.client.IDfSysObject;
import com.documentum.fc.common.DfException;
import com.documentum.fc.common.IDfId;
import com.documentum.fc.common.IDfList;
import com.documentum.operations.IDfExportNode;
import com.documentum.operations.IDfExportOperation;
import com.documentum.operations.IDfFile;
import com.documentum.operations.IDfImportNode;
import com.documentum.operations.IDfImportOperation;

public class DocOperations {
	
	public static void bulkImport( String srcFileOrDir, String destFolderPath, IDfSession session)
			 throws DfException {
		
		// step1: Check given destination folder exist in docbase
		
		 IDfFolder folder = null;
		 folder = session.getFolderByPath( destFolderPath );
		 if( folder == null ) {
			 Logger.getLogger("").info("Given folder doesn't Exist in DocBase" + destFolderPath );
			 return; 
		 }
	
		 // Step2: Create IDfImportOperation object
		 
		 IDfClientX clientx = new DfClientX();
		 IDfImportOperation impOperation = clientx.getImportOperation();
		 
		 // Step 3: Set Session and DestinationFolderID is must for Import Operation
		 
		 impOperation.setSession( session );
		 impOperation.setDestinationFolderId(folder.getObjectId());

	 
		 // Step 4: Import Files into the Repo
		 
		 IDfImportNode importNode = (IDfImportNode) impOperation.add( srcFileOrDir );
		 impOperation.execute();

	 } 
	
	
	public static void doExportMultipleObjects(String strFolderPath, String destDir, IDfSession session )
			 throws DfException {
		
		
		// Step1: Check given source folder exist in docbase
		
		 IDfFolder folder = null;
		 folder = session.getFolderByPath(strFolderPath);
		 if( folder == null ) {
			Logger.getLogger("").info("Folder or cabinet " + strFolderPath + " does not exist in the Docbase!");
			return;
		 }
		
		 String qualification = "select * from dm_document where FOLDER(ID('" + folder.getObjectId() + "'))";

		
		 // Step 2: Create IDfExportOperation object and set DestinationDirectory
		 
		 IDfClientX clientx = new DfClientX();
		 IDfExportOperation operation = clientx.getExportOperation();

		 operation.setDestinationDirectory(destDir);

		 IDfCollection col = null;
		 try {

		 IDfQuery q = clientx.getQuery(); 
		 
		 q.setDQL(qualification); 
		 col = q.execute((IDfSession) session, IDfQuery.DF_READ_QUERY); 

		 // Step 3: Iterate the collection Object and get Doc Object and export
		 while (col.next()) {
			 
		 String objName = col.getString("object_name");
		 IDfFile desFileDir = clientx.getFile( destDir + objName );
		 
		 
		 if (! desFileDir.exists()){
			 String id = col.getString("r_object_id");
			 IDfId docObjId = clientx.getId(id);
			 IDfSysObject docObj = (IDfSysObject) ((IDfSession) session).getObject(docObjId);
			 
			 // add the docObj to the operation
			 IDfExportNode node = (IDfExportNode)operation.add(docObj);
		  }
		 }
		 // Step 4: Execute the Operation
		 operation.execute();

		 Logger.getLogger("").info("The objects in " + strFolderPath + " have been exported to " + destDir);	 
		 }
		 finally {
			 if (col!= null)
			 col.close();
		 }	 	
		}
}
