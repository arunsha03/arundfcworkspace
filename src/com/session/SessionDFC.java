package com.session;


import java.util.logging.Logger;

import com.documentum.com.DfClientX;
import com.documentum.com.IDfClientX;
import com.documentum.fc.client.IDfClient;
import com.documentum.fc.client.IDfSession;
import com.documentum.fc.client.IDfSessionManager;
import com.documentum.fc.common.IDfLoginInfo;

public class SessionDFC {
	
	private IDfClientX clientx =null;
	private IDfSessionManager sMgr =null;
	private IDfClient client=null;
	private IDfLoginInfo loginInfoObj =null;
	
	public IDfSession createSessionManager(String repo, String user, String pass) throws Exception {
		clientx = new DfClientX();
		client = clientx.getLocalClient();
		sMgr = client.newSessionManager();
		loginInfoObj = clientx.getLoginInfo();
		loginInfoObj.setUser(user);
		loginInfoObj.setPassword(pass);
		loginInfoObj.setDomain(null);
		sMgr.setIdentity(repo, loginInfoObj);
		Logger.getLogger("").info("Session Created..!");
		return sMgr.getSession(repo);
	}

	public void releaseSession(IDfSession session) throws Exception {
		if (session != null) {
			sMgr = session.getSessionManager();
			sMgr.release(session);
			Logger.getLogger("").info("Session Released..!");
		}
	}

}
